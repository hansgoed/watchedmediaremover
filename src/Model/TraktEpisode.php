<?php

namespace App\Model;

class TraktEpisode
{
    private $id;
    private $showTvdbId;
    /**
     * @var int
     */
    private $seasonNumber;
    /**
     * @var int
     */
    private $episodeNumber;

    public function __construct(int $id, int $showTvdbId, int $seasonNumber, int $episodeNumber)
    {
        $this->id = $id;
        $this->showTvdbId = $showTvdbId;
        $this->seasonNumber = $seasonNumber;
        $this->episodeNumber = $episodeNumber;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getShowTvdbId(): int
    {
        return $this->showTvdbId;
    }

    /**
     * @return int
     */
    public function getSeasonNumber(): int
    {
        return $this->seasonNumber;
    }

    /**
     * @return int
     */
    public function getEpisodeNumber(): int
    {
        return $this->episodeNumber;
    }
}