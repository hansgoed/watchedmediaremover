<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SonarrEpisodeFileRepository")
 */
class SonarrEpisodeFile
{
    /**
     * @var int
     *
     * @ORM\Column()
     * @ORM\Id()
     */
    private $showTvdbId;

    /**
     * @var int
     *
     * @ORM\Column()
     * @ORM\Id()
     */
    private $seasonNumber;

    /**
     * @var int
     *
     * @ORM\Column()
     * @ORM\Id()
     */
    private $episodeNumber;

    /**
     * @var int
     *
     * @ORM\Column()
     */
    private $episodeFileId;

    public function __construct($showTvdbId, $seasonNumber, $episodeNumber, $episodeFileId)
    {
        $this->showTvdbId = $showTvdbId;
        $this->seasonNumber = $seasonNumber;
        $this->episodeNumber = $episodeNumber;
        $this->episodeFileId = $episodeFileId;
    }

    /**
     * @return int
     */
    public function getEpisodeFileId(): int
    {
        return $this->episodeFileId;
    }
}