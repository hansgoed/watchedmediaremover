<?php

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;
/**
 * @ORM\Entity()
 */
class Setting
{
    const TRAKT_ACCESS_TOKEN = 'TRAKT_ACCESS_TOKEN';
    const TRAKT_REFRESH_TOKEN = 'TRAKT_REFRESH_TOKEN';
    const TRAKT_LAST_REFRESH = 'TRAKT_LAST_REFRESH';
    const LAST_SYNCED_EPISODE = 'LAST_SYNCED_EPISODE';
    const LAST_SYNCED_MOVIE = 'LAST_SYNCED_MOVIE';

    public function __construct(string $key, ?string $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @var string
     * @ORM\Column(type="string", name="`key`")
     * @ORM\Id()
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(type="string", name="`value`")
     */
    private $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}