<?php

namespace App\Sonarr;

use App\Entity\SonarrEpisodeFile;

class Client
{
    private $apiKey = '04f7d9a1167d4257bcc1618d2ac4b156';

    public function getDownloadedEpisodes()
    {
        $client = new \GuzzleHttp\Client();
        $seriesResponse = $client->request('GET', 'http://docker.for.win.localhost:8989/api/series', [
            'query' => [
                'apikey' => $this->apiKey
            ]
        ]);

        $shows = json_decode($seriesResponse->getBody());

        $downloadedEpisodes = [];
        foreach ($shows as $show) {
            $episodesResponse = $client->request('GET', 'http://docker.for.win.localhost:8989/api/episode', [
                'query' => [
                    'apiKey' => $this->apiKey,
                    'seriesId' => $show->id
                ]
            ]);

            $episodes = json_decode($episodesResponse->getBody());
            foreach ($episodes as $episode) {
                if ($episode->episodeFileId > 0) {
                    $episode->show = $show;
                    $downloadedEpisodes[] = $episode;
                }
            }
        }

        return $downloadedEpisodes;
    }

    public function removeEpisode(SonarrEpisodeFile $episodeToRemove)
    {
        $client = new \GuzzleHttp\Client(['exceptions' => false]);
        $removeEpisodeResponse = $client->request('DELETE', 'http://docker.for.win.localhost:8989/api/episodeFile/' . $episodeToRemove->getEpisodeFileId(), [
            'query' => [
                'apikey' => $this->apiKey
            ]
        ]);

        if ($removeEpisodeResponse->getStatusCode() === 200) {
            return;
        }

        var_dump(json_decode($removeEpisodeResponse->getBody()));
    }
}