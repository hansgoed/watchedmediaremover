<?php

namespace App\Trakt;

use App\Entity\Setting;
use App\Model\TraktEpisode;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;

class TraktFetcher
{
    private $clientId = '5903b585bc3898a618079ed2833ffad7d12fff130bd786842327a100c1eb98b4';
    private $clientSecret = 'b8a507a11e992f48b8498fa62ae4616aae42b72d62c53ae671e90447392b46cd';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return TraktEpisode[]
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEpisodesToRemove(): array
    {
        $accessToken = $this->entityManager->find(Setting::class, Setting::TRAKT_ACCESS_TOKEN);
        $lastSyncedEpisodeSetting = $this->entityManager->find(Setting::class, Setting::LAST_SYNCED_EPISODE);

        $client = new Client();

        $limit = 25;
        if ($lastSyncedEpisodeSetting === null) {
            $limit = 2500;
        }

        $episodesToRemove = [];
        for ($i = 1; true; $i++) {
            if ($i > 1) {
                // Rate limit
                sleep(1);
            }

            $response = $client->request('GET', 'https://api.trakt.tv/sync/history/episodes', [
                'headers' => [
                    'content-type' => 'application/json',
                    'authorization' => 'Bearer ' . $accessToken->getValue(),
                    'trakt-api-version' => 2,
                    'trakt-api-key' => $this->clientId
                ],
                'query' => [
                    'page' => $i,
                    'limit' => $limit
                ]
            ]);

            $items = json_decode($response->getBody());
            foreach ($items as $item) {
                if ($lastSyncedEpisodeSetting !== null && $item->id === $lastSyncedEpisodeSetting->getValue()) {
                    break 2;
                }

                if ($item->show->ids->tvdb === null) {
                    continue;
                }

                $episodesToRemove[] = new TraktEpisode($item->id, $item->show->ids->tvdb, $item->episode->season, $item->episode->number);
            }

            if (count($items) < $limit) {
                break;
            }
        }

        return $episodesToRemove;
    }

    public function getMoviesToRemove()
    {
        $accessToken = $this->entityManager->find(Setting::class, Setting::TRAKT_ACCESS_TOKEN);
        $lastSyncedMovieSetting = $this->entityManager->find(Setting::class, Setting::LAST_SYNCED_MOVIE);

        $client = new Client();

        $limit = 25;
        if ($lastSyncedMovieSetting === null) {
            $limit = 2500;
        }

        $itemsToSync = [];
        for ($i = 1; true; $i++) {
            if ($i > 1) {
                // Rate limit
                sleep(1);
            }

            $response = $client->request('GET', 'https://api.trakt.tv/sync/history/movies', [
                'headers' => [
                    'content-type' => 'application/json',
                    'authorization' => 'Bearer ' . $accessToken->getValue(),
                    'trakt-api-version' => 2,
                    'trakt-api-key' => $this->clientId
                ],
                'query' => [
                    'page' => $i,
                    'limit' => $limit
                ]
            ]);

            $items = json_decode($response->getBody());
            foreach ($items as $item) {
                if ($lastSyncedMovieSetting !== null && $item->id === $lastSyncedMovieSetting->getValue()) {
                    break 2;
                }

                if ($item->movie->ids->tmdb === null) {
                    continue;
                }

                $itemsToSync[] = new TraktEpisode($item->id, $item->movie->ids->tmdb);
            }

            if (count($items) < $limit) {
                break;
            }
        }

        return $itemsToSync;
    }

    public function generateDeviceCode()
    {
        $client = new Client(['exceptions' => false]);

        $result = $client->request('POST', 'https://api.trakt.tv/oauth/device/code', [
            'headers' => ['content-type' => 'application/json'],
            'body' => json_encode(['client_id' => $this->clientId])
        ]);

        if ($result->getStatusCode() === 200) {
            return json_decode($result->getBody());
        }

        throw new \Exception('Couldn\'t fetch device code');
    }

    public function getAccessToken($deviceCode)
    {
        $client = new Client(['exceptions' => false]);
        $result = $client->request('POST', 'https://trakt.tv/oauth/device/token', [
            'headers' => ['content-type' => 'application/json'],
            'body' => json_encode([
                'code' => $deviceCode,
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret
            ])
        ]);

        if ($result->getStatusCode() === 400) {
            return null;
        }

        if ($result->getStatusCode() === 200) {
            return json_decode($result->getBody());
        }

        throw new \Exception('Did you deny? StatusCode: ' . $result->getStatusCode());
    }

    public function requiresAuthentication(): bool
    {
        $lastRefreshSetting = $this->entityManager->find(Setting::class, Setting::TRAKT_LAST_REFRESH);

        if ($lastRefreshSetting === null) {
            return true;
        }

        if ($lastRefreshSetting->getValue() - 7200 < 0) {
            return false;
        }

        if (!$this->reAuthenticate()) {
            return true;
        }

        return false;
    }

    /**
     * Use refresh token to get a new authentication token
     */
    private function reAuthenticate()
    {
        $refreshTokenSetting = $this->entityManager->find(Setting::class, Setting::TRAKT_REFRESH_TOKEN);

        if ($refreshTokenSetting === null) {
            throw new \Exception("Can't reauthenticate if authentication hasn't happened yet");
        }

        $client = new Client(['exceptions' => false]);
        $accessTokenResponse = $client->request('POST', 'https://api.trakt.tv/oauth/token', [
            'headers' => ['content-type' => 'application/json'],
            'body' => json_encode([
                'refresh_token' => $refreshTokenSetting->getValue(),
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri' => 'urn:ietf:wg:oauth:2.0:oob',
                'grant_type' => 'refresh_token'
            ])
        ]);

        if ($accessTokenResponse->getStatusCode() === 200) {
            $accessTokenResponse = json_decode($accessTokenResponse->getBody());

            $this->saveTokens($accessTokenResponse);
            return true;
        }

        return false;
    }

    public function saveTokens($accessTokenResponse)
    {
        $accessTokenSetting = $this->entityManager->find(Setting::class, Setting::TRAKT_ACCESS_TOKEN);
        if ($accessTokenSetting === null) {
            $accessTokenSetting = new Setting(Setting::TRAKT_ACCESS_TOKEN);
            $this->entityManager->persist($accessTokenSetting);
        }

        $refreshTokenSetting = $this->entityManager->find(Setting::class, Setting::TRAKT_REFRESH_TOKEN);
        if ($refreshTokenSetting === null) {
            $refreshTokenSetting = new Setting(Setting::TRAKT_REFRESH_TOKEN);
            $this->entityManager->persist($refreshTokenSetting);
        }

        $lastRefreshSetting = $this->entityManager->find(Setting::class, Setting::TRAKT_LAST_REFRESH);
        if ($lastRefreshSetting === null) {
            $lastRefreshSetting = new Setting(Setting::TRAKT_LAST_REFRESH);
            $this->entityManager->persist($lastRefreshSetting);
        }

        $accessTokenSetting->setValue($accessTokenResponse->access_token);
        $refreshTokenSetting->setValue($accessTokenResponse->refresh_token);
        $lastRefreshSetting->setValue($accessTokenResponse->created_at);

        $this->entityManager->flush();
    }
}