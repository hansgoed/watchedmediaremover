<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190314143243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE setting ("key" VARCHAR(255) NOT NULL, "value" VARCHAR(255) NOT NULL, PRIMARY KEY("key"))');
        $this->addSql('CREATE TABLE sonarr_episode_file (show_tvdb_id VARCHAR(255) NOT NULL, season_number VARCHAR(255) NOT NULL, episode_number VARCHAR(255) NOT NULL, episode_file_id VARCHAR(255) NOT NULL, PRIMARY KEY(show_tvdb_id, season_number, episode_number))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE setting');
        $this->addSql('DROP TABLE sonarr_episode_file');
    }
}
