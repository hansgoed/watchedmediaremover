<?php

namespace App\Command;

use App\Entity\SonarrEpisodeFile;
use App\Model\TraktEpisode;
use App\Sonarr\Client;
use App\Trakt\TraktFetcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveWatchedMediaCommand extends Command
{
    /**
     * @var TraktFetcher Helper to retrieve watched data from Trakt
     */
    private $traktFetcher;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Client
     */
    private $sonarrClient;

    public function __construct(
        TraktFetcher $traktFetcher,
        EntityManagerInterface $entityManager,
        Client $sonarrClient
    ) {
        $this->traktFetcher = $traktFetcher;
        $this->entityManager = $entityManager;
        $this->sonarrClient = $sonarrClient;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:remove_watched_media')
            ->setDescription('Remove watched media by fetching from Trakt, remove in Sonarr & Radarr, send update to Kodi');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->traktFetcher->requiresAuthentication()) {
            $this->authenticate($output);
        }

        $episodesToRemove = $this->traktFetcher->getEpisodesToRemove();

        $this->updateSonarrReferences();
        $this->removeEpisodesFromSonarr($episodesToRemove);

        // $moviesToRemove = $this->traktFetcher->getMoviesToRemove();
    }

    private function authenticate(OutputInterface $output)
    {
        $deviceCodeResult = $this->traktFetcher->generateDeviceCode();

        $output->writeln(json_encode($deviceCodeResult));

        $output->writeln('<question>Open ' . $deviceCodeResult->verification_url . '</question>');
        $output->writeln('<question>Enter code: ' . $deviceCodeResult->user_code . '</question>');
        $output->writeln('<question>The app will continue after you have entered the code</question>');

        $intervalTime = $deviceCodeResult->interval;

        $amountOfSteps = $deviceCodeResult->expires_in / $intervalTime;

        $progressBar = new ProgressBar($output, $amountOfSteps);
        $progressBar->setFormat('[%bar%] %remaining% left');

        $progressBar->start();

        $accessTokenResponse = null;
        for ($i = 0; $i < $amountOfSteps; $i++)  {
            $accessTokenResponse = $this->traktFetcher->getAccessToken($deviceCodeResult->device_code);
            if ($accessTokenResponse !== null) {
                break;
            }

            sleep($deviceCodeResult->interval);
            $progressBar->advance();
        }
        $progressBar->finish();
        $output->writeln(' ');

        if ($accessTokenResponse === null) {
            throw new \Exception('You took too long to enter the code..');
        }

        $this->traktFetcher->saveTokens($accessTokenResponse);

        Return $accessTokenResponse->access_token;
    }

    private function updateSonarrReferences()
    {
        $downloadedEpisodes = $this->sonarrClient->getDownloadedEpisodes();

        foreach ($downloadedEpisodes as $downloadedEpisode) {
            $sonarrEpisodeFileRepository = $this->entityManager->getRepository(SonarrEpisodeFile::class);
            $sonarrEpisode = $sonarrEpisodeFileRepository->findOneBy([
                'showTvdbId' => $downloadedEpisode->show->tvdbId,
                'seasonNumber' => $downloadedEpisode->seasonNumber,
                'episodeNumber' => $downloadedEpisode->episodeNumber
            ]);

            if ($sonarrEpisode !== null) {
                continue;
            }

            $sonarrEpisode = new SonarrEpisodeFile(
                $downloadedEpisode->show->tvdbId,
                $downloadedEpisode->seasonNumber,
                $downloadedEpisode->episodeNumber,
                $downloadedEpisode->episodeFileId
            );

            $this->entityManager->persist($sonarrEpisode);
        }

        $this->entityManager->flush();
    }

    /**
     * @param TraktEpisode[] $episodesToRemove
     */
    private function removeEpisodesFromSonarr(array $episodesToRemove)
    {
        $sonarrEpisodeFileRepository = $this->entityManager->getRepository(SonarrEpisodeFile::class);

        foreach ($episodesToRemove as $episodeToRemove) {
            $sonarrEpisodeFile = $sonarrEpisodeFileRepository->findOneBy([
                'showTvdbId' => $episodeToRemove->getShowTvdbId(),
                'seasonNumber' => $episodeToRemove->getSeasonNumber(),
                'episodeNumber' => $episodeToRemove->getEpisodeNumber()
            ]);

            if ($sonarrEpisodeFile === null) {
                continue;
            }

            $this->sonarrClient->removeEpisode($sonarrEpisodeFile);
            $this->entityManager->remove($sonarrEpisodeFile);
        }
    }
}