<?php

namespace App\Repository;

use App\Entity\SonarrEpisodeFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SonarrEpisodeFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SonarrEpisodeFile::class);
    }
}